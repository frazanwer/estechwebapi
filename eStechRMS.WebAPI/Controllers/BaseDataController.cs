﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using eStechRMS.WebAPI.Helpers;
using eStechRMS.WebAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eStechRMS.WebAPI.Controllers
{

    [Produces("application/json")]
    [Route("api/[controller]")]
    [EnableCors("charppolicy")]
    public class BaseDataController : Controller
    {
        LINQtoDataTable linqdt;


        // GET: api/GetUsers/5
        [HttpGet]
        [Route("getusers")]
        public IEnumerable<ADM_User> getusers(string device_no)
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 1, Office_ID = 0, Device_No = device_no, Area_ID = 0 };
                return db.Query<ADM_User>
                (sql, values).AsEnumerable();
            }
        }


        [HttpGet]
        [Route("getdeviceid")]
        public IEnumerable<ADM_Device> getdeviceid(string device_no,int office_id)
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 0, Office_ID = 1, Device_No = device_no, Area_ID = 0 };
                return db.Query<ADM_Device>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getregions
        [HttpGet]
        [Route("getregions")]
        public IEnumerable<ADM_Region> getregions()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 2, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<ADM_Region>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getareas
        [HttpGet]
        [Route("getareas")]
        public IEnumerable<ADM_Area> getareas()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 3, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<ADM_Area>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getcities
        [HttpGet]
        [Route("getcities")]
        public IEnumerable<ADM_City> getcities()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 4, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<ADM_City>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getpaymentmethods
        [HttpGet]
        [Route("getpaymentmethods")]
        public IEnumerable<ADM_Payment_Method> getpaymentmethods()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 5, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<ADM_Payment_Method>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getitemcategorytypes
        [HttpGet]
        [Route("getitemcategorytypes")]
        public IEnumerable<SCM_ItemCategoryType> getitemcategorytypes()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 6, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_ItemCategoryType>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getitemmeasurementtypes
        [HttpGet]
        [Route("getitemmeasurementtypes")]
        public IEnumerable<SCM_Item_Measurement_Type> getitemmeasurementtypes()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 7, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_Item_Measurement_Type>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getitemcategory
        [HttpGet]
        [Route("getitemcategory")]
        public IEnumerable<SCM_Item_Category> getitemcategory()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 8, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_Item_Category>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getitemsubcategories
        [HttpGet]
        [Route("getitemsubcategories")]
        public IEnumerable<SCM_Item_SubCategory> getitemsubcategories()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 9, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_Item_SubCategory>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getpricelists
        [HttpGet]
        [Route("getpricelists")]
        public IEnumerable<SCM_PriceList> getpricelists()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 10, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_PriceList>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getitems
        [HttpGet]
        [Route("getitems")]
        public IEnumerable<SCM_Item> getitems()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 11, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_Item>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getcustomercategory
        [HttpGet]
        [Route("getcustomercategory")]
        public IEnumerable<SCM_CustomerCategory> getcustomercategory()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 12, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_CustomerCategory>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getitemstock
        [HttpGet]
        [Route("getitemstock")]
        public IEnumerable<SCM_Item_Stock> getitemstock()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 13, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_Item_Stock>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getitempricelists
        [HttpGet]
        [Route("getitempricelists")]
        public IEnumerable<SCM_ItemPriceList> getitempricelists()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 14, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_ItemPriceList>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getcustomercategorypricelist
        [HttpGet]
        [Route("getcustomercategorypricelist")]
        public IEnumerable<SCM_CustomerCategoryPriceList> getcustomercategorypricelist()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 15, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_CustomerCategoryPriceList>
                (sql, values).AsEnumerable();
            }
        }

        // GET: api/getitemoffices
        [HttpGet]
        [Route("getitemoffices")]
        public IEnumerable<SCM_ItemOffice> getitemoffices()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 16, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_ItemOffice>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getoffices
        [HttpGet]
        [Route("getoffices")]
        public IEnumerable<ADM_OFFICE> getoffices()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 17, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<ADM_OFFICE>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getoffices
        [HttpGet]
        [Route("getcustomers")]
        public IEnumerable<SCM_Customer> getcustomers()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 18, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<SCM_Customer>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getcountries
        [HttpGet]
        [Route("getcountries")]
        public IEnumerable<ADM_Country> getcountries()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 19, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<ADM_Country>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getprovinces
        [HttpGet]
        [Route("getprovinces")]
        public IEnumerable<ADM_Province> getprovinces()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 20, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<ADM_Province>
                (sql, values).AsEnumerable();
            }
        }
        // GET: api/getday
        [HttpGet]
        [Route("getday")]
        public IEnumerable<ADM_Day> getday()
        {

            using (IDbConnection db = new SqlConnection(Startup.ConnectionString))
            {
                var sql = "exec RMS_IntialServerData @TableID,@Office_ID,@Device_No,@Area_ID";
                var values = new { TableID = 21, Office_ID = 0, Device_No = 0, Area_ID = 0 };
                return db.Query<ADM_Day>
                (sql, values).AsEnumerable();
            }
        }
        // POST: api/Users
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{

        //}

        [HttpPost]
        [Route("SaveData")]
        public ActionResult<data_model> SaveData([FromBody]data_model param)
        {
            try
            {
                linqdt = new LINQtoDataTable();
                //    SqlParameter[] dbparam = new SqlParameter[4];
                //    dbparam[0] = new SqlParameter("@orders", linqdt.LINQResultToDataTable(param.orders));
                //    dbparam[1] = new SqlParameter("@orderdetails", linqdt.LINQResultToDataTable(param.orderDetails));
                //    dbparam[2] = new SqlParameter("@salepayments", linqdt.LINQResultToDataTable(param.salepayments));
                //    dbparam[3] = new SqlParameter("@orderImageData", linqdt.LINQResultToDataTable(param.orderImgLoc));

                //    SqlHelper.ExecuteNonQuery("FFM_Daily_Sync", dbparam);
                DataTable dtOrders = linqdt.LINQResultToDataTable(param.orders);
                DataTable dtOrderDetails = linqdt.LINQResultToDataTable(param.orderDetails);


                SyncSales.SaveTransaction(param);
                data_model model = new data_model();
                model.orders= SyncSales.GetOrdersList(param);

                //, order.Sale_Order_ID, order.SO_Date, order.Office_Code, order.Sale_Envoy, order.Customer_ID, order.Contact_Person_ID, order.Delivery_Date, order.Shipping_Method, order.Payment_ID, order.Freight_Term, order.Total_Cost, order.Total_Discount, order.Total_Tax, order.Freight_Chrgs, order.Total_Amount, order.paid_Amount, order.Reorder_ID, order.Quotation_ID, order.Order_Type, order.Remarks, order.Cancel, order.CompanyID, order.ManualDiscount, DateTime.Now, order.Created_By, false, order.SOGUID, order.IsDirect, order.SD_Rate, order.SD_Amount, order.specialDiscount, order.exchange, 1, order.RefCustomerID";

                return Ok(model);

            }
            catch (Exception ex)
            {
                String er = ex.Message;
                return Ok(ex.Message);// NoContent();
            }



        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
