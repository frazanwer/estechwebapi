﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_Customer
	{
		[Key]
		public int? Customer_ID { get; set; }
		[MaxLength(300)]
		public string Customer_Name { get; set; }
		[MaxLength(50)]
		public string Phone { get; set; }
		[MaxLength(50)]
		public string Email { get; set; }
 
		public int? CustomerCategoryId { get; set; }
		public int? AreaId { get; set; }
		public int? City_ID { get; set; }	 
		public Decimal? NetPayable { get; set; }
  
	}

}
