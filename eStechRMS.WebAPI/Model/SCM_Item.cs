﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_Item
	{
		[Key]
		public int? Item_Code { get; set; }
		[MaxLength(50)]
		public string Barcode { get; set; }
		[MaxLength(500)]
		public string Item_Name { get; set; }
		public Decimal? Cost { get; set; }
		public Decimal? Unit_Price { get; set; }
		public int? Measurement_Unit_ID { get; set; }
		public int? Category_Code { get; set; }
		public int? SubCategory_Code { get; set; }
		public int? Packing_Type_ID { get; set; }		
		public decimal? Packing_Quantity { get; set; }
		public int? CategoryTypeID { get; set; }
		public decimal? ItemWeight { get; set; }
		public decimal? Reorder_Quantity { get; set; }
		public string ItemLongCode { get; set; }
		public bool? Status { get; set; }
		public DateTime? ExpiryDate { get; set; }
		
		//

	}
}
