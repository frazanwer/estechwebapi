﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_ItemPriceList
	{
		[Key]
		public int? ItemPriceListID { get; set; }
		public int? PriceListID { get; set; }
		public int? Item_Code { get; set; }
		public Decimal? Sale_Price { get; set; }
		 
	}
}
