﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class ADM_Country
	{
		[Key]
		public int Country_Id { get; set; }
		[MaxLength(200)]
		public string  Name { get; set; } 
 
	}
}

