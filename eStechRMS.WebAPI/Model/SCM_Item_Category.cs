﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_Item_Category
	{
		[Key]
		public int? CategoryTypeID { get; set; }
		[MaxLength(300)]
		public string Category_Name { get; set; }
		[MaxLength(10)]
		public string Abbreviation { get; set; }
		 
	}

}
