﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class ADM_User
	{

		[Key]
		public int? User_ID { get; set; }
		[MaxLength(100)]
		public string Login { get; set; }
		[MaxLength(50)]
		public string Password_Value { get; set; }
		[MaxLength(4)]
		public string PIN_Value { get; set; }
		[MaxLength(50)]
		public string Device_No { get; set; } 
		public int? Office_Code { get; set; }
		[MaxLength(100)]
		public string Office_Name { get; set; }
		[MaxLength(200)]
		public string Staff_Name { get; set; }
		[MaxLength(200)]
		public string Designation { get; set; }
		[Column(TypeName = "datetime")]
		public DateTime? Pass_Exp_Date { get; set; }
		[MaxLength(5)]
		public string AppVersion { get; set; }
		public int? Role_Id { get; set; }

		 
	}
}
