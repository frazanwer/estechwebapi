﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
   public class ADM_Device
    {
		
		
		[Key]
		[MaxLength(50)]
		public string DeviceNo { get; set; }

		public int? DeviceId { get; set; }


	}
}
