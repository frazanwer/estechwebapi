﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class ADM_Payment_Method
	{
		[Key]
		public int? Method_ID { get; set; }
		[MaxLength(100)]
		public string Method_Name { get; set; }
		 
	}
}
