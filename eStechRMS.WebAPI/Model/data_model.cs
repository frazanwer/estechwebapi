﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
    public class data_model
    {
        public List<SCM_Sale_Order> orders { get; set; }
        public List<SCM_Sale_Order_Detail> orderDetails { get; set; }
    }
}
