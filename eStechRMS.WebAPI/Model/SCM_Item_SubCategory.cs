﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_Item_SubCategory
	{
		[Key]
		public int? SubCategory_Code { get; set; }
		[MaxLength(500)]
		public string SubCategory_Name { get; set; }
		public int? Category_Code { get; set; }
		 
	}

}
