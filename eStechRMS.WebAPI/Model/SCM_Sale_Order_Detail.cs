﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_Sale_Order_Detail
	{
		[Key]
		public int Sale_Terminal_Detail_ID { get; set; }
	    public int? Sale_Detail_ID { get; set; }
		[Required]
		public int Sale_Terminal_ID { get; set; }
		public int? Sale_Order_ID { get; set; }
		public string Sale_Code { get; set; }
		public int? Item_Code { get; set; }
		[MaxLength(50)]
		public string Item_Detail { get; set; }
		public int? Quantity { get; set; }
		public Decimal? Unit_Price { get; set; }
		public Decimal? Sale_Cost { get; set; }
	 
		public Decimal? Discount_Rate { get; set; }
		public Decimal? Discount_Amount { get; set; }
		public Decimal? Tax_Rate { get; set; }
		public Decimal? Tax_Amount { get; set; }
		public bool? isInvoiced { get; set; }
		[Required]
		public bool Scheme { get; set; }
		public int? TaxRateID { get; set; }

	 
	}
}
