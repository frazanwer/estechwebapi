﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_Item_Stock
	{
		[Key]
		public int? Stock_ID { get; set; }
		public int? Item_Code { get; set; }
		public Decimal? Quantity { get; set; }
		public int? Office_Code { get; set; }
		 
	}

}
