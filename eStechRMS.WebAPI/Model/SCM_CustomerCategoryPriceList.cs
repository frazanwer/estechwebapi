﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_CustomerCategoryPriceList
	{
		[Key]
		public int? CustomerCategoryPriceListID { get; set; }
		public int? CustomerCategoryID { get; set; }
		public int? PriceListID { get; set; }
		 
	}
}
