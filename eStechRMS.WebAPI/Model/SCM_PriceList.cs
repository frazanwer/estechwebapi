﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_PriceList
	{
		[Key]
		public int? PriceListID { get; set; }
		[MaxLength(200)]
		public string PriceListName { get; set; }
		 
	}

}
