﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class ADM_Region
	{
		[Key]
		public int RegionId { get; set; }
		[MaxLength(500)]
		public string RegionName { get; set; } 
		public int? ProvinceId { get; set; }
	   
	}

}
