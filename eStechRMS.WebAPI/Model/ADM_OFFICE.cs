﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class ADM_OFFICE
	{
		[Key]
		public int? Office_Code { get; set; }
		[MaxLength(200)]
		public string Office_Name { get; set; }
		 
	}
}
