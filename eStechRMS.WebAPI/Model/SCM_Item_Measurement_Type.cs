﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_Item_Measurement_Type
	{
		[Key]
		public int? Measurement_Unit_ID { get; set; }
		[MaxLength(300)]
		public string Measurement_Unit { get; set; }
		[MaxLength(10)]
		public string Abbr { get; set; }
		 
	}
}
