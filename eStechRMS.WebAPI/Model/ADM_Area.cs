﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class ADM_Area
	{
		[Key]
		public int AreaId { get; set; }
		[MaxLength(200)]
		public string AreaName { get; set; }
		[MaxLength(350)]
		public string AreaDetail { get; set; }
		public int? RegionId { get; set; }
		public int? CityId { get; set; }
		public bool? Status { get; set; }
		 
	 
	}
}

