﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class ADM_City
	{
		[Key]
		public int? City_Code { get; set; }
		[MaxLength(500)]
		public string Name { get; set; }
		public int? DISTRICT_CODE { get; set; }
		public int? Region_ID { get; set; }
		public int? Province_ID { get; set; }

	}
}
