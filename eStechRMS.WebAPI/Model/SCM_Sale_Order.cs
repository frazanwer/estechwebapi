﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{

	public class SCM_Sale_Order
	{
		[Key]
		public int Sale_Terminal_ID { get; set; }
		public int? Sale_Order_ID { get; set; }
		[Column(TypeName = "datetime")]
		public DateTime? SO_Date { get; set; }
		[MaxLength(50)]
		public string SO_NO { get; set; }
		public string Sale_Code { get; set; }
		public int? CompanyID { get; set; }
		public int? Office_Code { get; set; }
		public int? Customer_ID { get; set; }
		public Decimal? Total_Cost { get; set; }
		public Decimal? Total_Discount { get; set; }
		public Decimal? Pre_Tax_Amount { get; set; }
		public Decimal? Total_Tax { get; set; }
		public Decimal? SpecialDiscount { get; set; }
		public Decimal? Total_Amount { get; set; }
		[MaxLength(500)]
		public string Remarks { get; set; }
		[Required]
		public bool Cancel { get; set; }
		public int? Day_ID { get; set; }
		public int? Shift_ID { get; set; }
		public Decimal? Paid_Amount { get; set; }
		
		[Required]
		public bool Scheme { get; set; }
		public int? Status { get; set; }
		[Column(TypeName = "datetime")]
		public DateTime? Action_Date { get; set; }
		public int? Action_By { get; set; }


		 
		 
	}
}
