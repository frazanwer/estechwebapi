﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI.Models
{
	public class SCM_CustomerCategory
	{
		[Key]
		public int? CustomerCategoryId { get; set; }
		[MaxLength(500)]
		public string CustomerCategoryName { get; set; }
		 
	}
}
