﻿using Dapper;
using eStechRMS.WebAPI.Helpers;
using eStechRMS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace eStechRMS.WebAPI
{
    public class SyncSales
    {
        public static int SaveTransaction(data_model list)
        {
            int id = 0;
            try
            { 
                DataTable dtorders = list.orders.ToDataTable();
                DataTable dtorderDetails = list.orderDetails.ToDataTable();
                using (var connection = new SqlConnection(Startup.ConnectionString))
                {
                    //connection.Open();
                    //connection.BeginTransaction();
                    id = connection.ExecuteScalar<int>("USP_SaveTransaction",
                        new { @orders = dtorders, @orderDetail = dtorderDetails }, null, 0, CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                 
            }

            return id;
        }

        public static List<SCM_Sale_Order> GetOrdersList(data_model list)
        {
            DataTable dtorders = list.orders.ToDataTable();
            using (var connection = new SqlConnection(Startup.ConnectionString))
            {
                return connection.Query<SCM_Sale_Order>("[USP_GetSavedTransaction]",
                         new { @orders = dtorders }, null,false, 0, CommandType.StoredProcedure
                       ).ToList();

            }
        }
       
    }
}
